package com.login.simple.login.domain;

/**
 * Created by Ahdzlee on 7/29/2015.
 */
public class LoginService {

    private static final String MY_USERNAME = "ahdzlee";
    private static final String MY_PASSWORD = "123456";

    public boolean login(String username, String password) {
        return username.equals(MY_USERNAME) && password.equals(MY_PASSWORD);
    }
}
