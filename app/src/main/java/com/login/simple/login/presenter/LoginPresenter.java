package com.login.simple.login.presenter;

import com.login.simple.login.LoginActivity;
import com.login.simple.login.R;
import com.login.simple.login.domain.LoginService;
import com.login.simple.login.view.ILoginView;

/**
 * Created by Ahdzlee on 7/29/2015.
 */
public class LoginPresenter {

    private ILoginView mView;
    private LoginService mService;

    public LoginPresenter(ILoginView view, LoginService service) {
        mView = view;
        mService = service;
    }

    public void onLoginClicked() {
    }
}
