package com.login.simple.login.util;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Ahdzlee on 7/29/2015.
 */
public class ActivityUtil {

    public static void goToNextActivity(Context context, Class clz) {
        Intent intent = new Intent(context, clz);
        context.startActivity(intent);
    }
}
