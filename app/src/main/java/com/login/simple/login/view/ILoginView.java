package com.login.simple.login.view;

/**
 * Created by Ahdzlee on 7/29/2015.
 */
public interface ILoginView {
    String getUsername();

    void showUsernameError(int resId);
}
