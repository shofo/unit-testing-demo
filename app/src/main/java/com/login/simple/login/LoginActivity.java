package com.login.simple.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.login.simple.login.domain.LoginService;
import com.login.simple.login.presenter.LoginPresenter;
import com.login.simple.login.util.ActivityUtil;
import com.login.simple.login.view.ILoginView;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    private EditText mEtUsername;
    private EditText mEtPassword;

    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEtUsername = (EditText) findViewById(R.id.etUsername);
        mEtPassword = (EditText) findViewById(R.id.etPassword);

        mPresenter = new LoginPresenter(this, new LoginService());
    }

    public void onLoginClicked(View v) {
        mPresenter.onLoginClicked();

        String username = mEtUsername.getText().toString();
        String password = mEtPassword.getText().toString();

        if (username.isEmpty()) {
            mEtPassword.setError(getString(R.string.password_error));
            return;
        }

        if (password.isEmpty()) {
            mEtPassword.setError(getString(R.string.password_error));
            return;
        }

        boolean isLoginSuccessful = new LoginService().login(username, password);
        if (isLoginSuccessful) {
            ActivityUtil.goToNextActivity(this, MainActivity.class);
        } else {
            Toast.makeText(this, R.string.login_error, Toast.LENGTH_SHORT).show();
        }
    }

    public void onClearBtnClicked(View view) {
        mEtUsername.setText("");
        mEtPassword.setText("");

        mEtUsername.setError(null);
        mEtPassword.setError(null);
    }

    public void onExitBtnClicked(View view) {
        this.finish();
    }

    @Override
    public String getUsername() {
        return mEtUsername.getText().toString();
    }

    @Override
    public void showUsernameError(int resId) {
        mEtUsername.setError(getString(resId));
    }
}
